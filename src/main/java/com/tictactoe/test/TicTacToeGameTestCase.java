package com.tictactoe.test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by danielvera on 3/24/16.
 */
public class TicTacToeGameTestCase {

    //private WebDriver driver;
    private RemoteWebDriver driver;
    private String baseUrl = "http://tictactoedv64.azurewebsites.net/";
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    private String platform_name = "mac";
    private String browser_name = "firefox";
    private String browser_version = "35";

    private String sauceLabsUSER = "chuwito";
    private String sauceLabsKEY = "b1f50471-6bc6-474a-baf1-ef62b0d130d1";
    private String sauceLabsURL = "http://" + sauceLabsUSER + ":" + sauceLabsKEY + "@ondemand.saucelabs.com:80/wd/hub";

    private long timeToWaitInGame = 5000;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        setUpRemoteServer();
    }

    @Test
    public void testCase1() throws Exception
    {
        String ramdomString = randomString();
        String ramdomUser = "Selenium user: " + ramdomString;
        String ramdomEmail = ramdomString + "@prueba.com";

        long startTime = System.currentTimeMillis();
        driver.get(baseUrl);

        //Register player 1
        driver.findElement(By.linkText("Register")).click();
        driver.findElement(By.name("name")).clear();
        driver.findElement(By.name("name")).sendKeys(ramdomUser);
        driver.findElement(By.name("email")).clear();
        driver.findElement(By.name("email")).sendKeys(ramdomEmail);
        driver.findElement(By.name("password")).clear();
        driver.findElement(By.name("password")).sendKeys("123456");
        driver.findElement(By.name("password_confirmation")).clear();
        driver.findElement(By.name("password_confirmation")).sendKeys("123456");
        driver.findElement(By.cssSelector("button.btn.btn-primary")).click();

        driver.findElement(By.linkText("Lobby")).click();
        driver.findElement(By.cssSelector("button.btn.btn-default")).click();
        driver.findElement(By.cssSelector("button.btn.btn-primary")).click();

        Thread.sleep(timeToWaitInGame);

        driver.findElement(By.linkText("Lobby")).click();
        while (driver.getPageSource().contains("Remove")) {
            driver.findElement(By.cssSelector("button.btn.btn-danger")).click();
        }
        driver.findElement(By.linkText(ramdomUser)).click();
        driver.findElement(By.linkText("Logout")).click();

        long endTime = System.currentTimeMillis();
        double totalTime = ((endTime - startTime) - timeToWaitInGame) * 0.001;

        System.out.println("-> Total test time: " + totalTime + " seconds for user: " + ramdomUser);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private void setUpRemoteServer()
    {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        if (platform_name.equalsIgnoreCase("mac")) {
            capabilities.setPlatform(Platform.YOSEMITE);
        }
        if (platform_name.equalsIgnoreCase("win8")) {
            capabilities.setPlatform(Platform.WIN8);
        }
        if (platform_name.equalsIgnoreCase("win8_1")) {
            capabilities.setPlatform(Platform.WIN8_1);
        }
        if (platform_name.equalsIgnoreCase("linux")) {
            capabilities.setPlatform(Platform.LINUX);
        }
        capabilities.setBrowserName(browser_name);
        capabilities.setVersion(browser_version);
        capabilities.setCapability("video", "False");

        try {
            driver = new RemoteWebDriver(new URL(sauceLabsURL),capabilities);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        //driver = new FirefoxDriver();
        //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

    private String randomString()
    {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }
}
